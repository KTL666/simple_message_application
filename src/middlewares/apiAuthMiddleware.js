const auth = include('src/util/auth')

module.exports = (req, res, next) => {

    function on_unauthorizaed() {
        return res.status(401).json({ message: 'Unauthorized!' });
    }
    function on_token_expire() {
        return res.status(401).json({ message: 'token expire' });
    }
    auth(req.headers['authorization'], req, next, on_unauthorizaed, on_token_expire)
};