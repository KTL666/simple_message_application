module.exports = {
	async up(db, client) {
		await db.createCollection('chatrooms')
	},

	async down(db, client) {
		await db.dropCollection('chatrooms')
	}
};
