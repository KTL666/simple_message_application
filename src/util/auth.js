var jwt = require('jsonwebtoken');
const config = include('config');
var { redis_client } = include('src/services/connection.js')

module.exports = (auth_header, req, next, on_unauthorizaed, on_token_expire) => {
	try {
		const token = auth_header.split(' ')[1];
		jwt.verify(token, config.auth.private_key, (err, decode) => {
			if (err) {
				on_unauthorizaed()
			} else {
				if (Date.now() / 1000 - decode.iat > config.auth.jwt_expiry_ms / 1000) {
					on_token_expire()
				} else {
					req.uuid = decode.uuid
					redis_client.set(req.uuid, Date.now(), 'PX', config.auth.jwt_expiry_ms)
					next();
				}
			}
		});
	} catch (e) {
		on_unauthorizaed()
	}
};