const auth = include('src/util/auth')

module.exports = (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Max-Age', 600)
    res.header('Access-Control-Expose-Headers', 'X-My-Custom-Header')
    next()
};