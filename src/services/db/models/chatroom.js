var { mongoose } = include('src/services/connection')

var chatroomSchema = mongoose.Schema({
    name: String,
    member_ids: [String],
    messages: [{
        type: new mongoose.Schema({
            sender: String,//require('mongodb').ObjectId,
            content: String
        }, { timestamps: true })
    }]
}, { timestamps: true });
var Chatroom = mongoose.model("chatrooms", chatroomSchema);

module.exports = Chatroom;