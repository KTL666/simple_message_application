global.base_dir = __dirname;
global.abs_path = (path) => {
	return base_dir + path;
}
global.include = (file) => {
	return require(abs_path('/' + file));
}
require('dotenv').config({ path: `.env/.env.${process.argv[2]}` })

var express = require('express');
var bodyParser = require('body-parser')
var app = express();

const cors_middleware = include('src/middlewares/corsMiddleware')
app.use(cors_middleware)

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
	extended: true
}));

var userRouter = require('./src/routes/user');
app.use('/user', userRouter);

var api_auth_middleware = include('src/middlewares/apiAuthMiddleware')
app.use(api_auth_middleware)

app.get('/', (req, res) => {
	res.json("Hello world!");
});

var chatroomRouter = require('./src/routes/chatroom');
app.use('/chatroom', chatroomRouter);

const socket = require('socket.io')
const http = require('http')
const server = http.createServer(app) // use express to handle http server
const io = socket(server, {
	cors: {
		origin: "*",
		methods: ["GET", "POST"]
	}
});
server.listen(process.env.DEPLOY_PORT);


const kafka = include('src/services/kafka')
const socket_auth_middleware = include('/src/middlewares/socketAuthMiddleware')
io.use(socket_auth_middleware)
io.sockets.on('connection', function (socket) {
	socket.emit('message', { 'message': 'hello world' });
	kafka(socket)
});
