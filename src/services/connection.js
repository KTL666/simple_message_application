var mongoose = require('mongoose');
mongoose.connect(`mongodb://${process.env.MONGODB_HOST}:${process.env.MONGODB_PORT}/${process.env.MONGODB_DB_NAME}`).then(() => {
	console.log('mongoose_client connect successfully!')
});

var redis = require('redis');
redis_client = redis.createClient({ url: `redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}` })
redis_client.connect();

redis_client.on('error', () => {
	console.log(`redis fail to connect redis://${process.env.REDIS_HOST}:${process.env.REDIS_PORT}`)
}).on('ready', () => {
	console.log('redis connect successfully!')
});

module.exports = { mongoose, redis_client };