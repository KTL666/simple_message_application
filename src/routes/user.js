var express = require('express');
var crypto = require('crypto');
var jwt = require("jsonwebtoken");
var { redis_client } = include('src/services/connection')
var api_auth_middleware = include('src/middlewares/apiAuthMiddleware')


var User = include('src/services/db/models/user');
const config = include('config');

var router = express.Router();

router.post('/registration', (req, res) => {
	var userInfo = req.body;

	if (!userInfo.name || !userInfo.password) {
		res.status(400).json("Sorry, you provided worng info")
	} else {

		var hashed_password = crypto.pbkdf2Sync(
			userInfo.password,
			config.auth.salt,
			config.auth.iterations,
			config.auth.keylen,
			config.auth.digest).toString(`hex`);

		var newUser = new User({
			name: userInfo.name,
			password: hashed_password,
		});

		newUser.save((err, User) => {
			if (err)
				res.status(500).json("Database error");
			else
				res.status(301).redirect('/login');
		});
	}
});


router.post('/login', (req, res) => {
	var userInfo = req.body;
	if (!userInfo.name || !userInfo.password) {
		res.status(400).json("Sorry, you provided worng info")
	} else {
		User.findOne({ name: userInfo.name }, (err, user) => {
			if (user === null) {
				return res.status(400).send({
					message: "User not found."
				});
			}
			else {

				var hashed_password = crypto.pbkdf2Sync(userInfo.password,
					config.auth.salt,
					config.auth.iterations,
					config.auth.keylen,
					config.auth.digest).toString(`hex`);
				if (user.password == hashed_password) {
					const token = jwt.sign({ uuid: user._id.toString(), hashed_password }, config.auth.private_key);
					redis_client.set(user._id.toString(), Date.now(), 'PX', config.auth.jwt_expiry_ms)
					res.status(200).json({ token });
				}
				else {
					return res.status(400).send({
						message: "Wrong Password"
					});
				}
			}
		});
	}
});

router.get('/chatroom_list', (req, res) => {
	User.findOne({ id: req.uuid }, (err, user) => {
		if (user === null) {
			return res.status(400).send({
				message: "User not found."
			});
		}
		else {
			res.status(200).send(user.chatrooms);
		}
	});
});

// router.use(api_auth_middleware)

router.get('/logout', (req, res) => {
	redis_client.del(req.uuid).then(() => {
		// res.status(301).redirect('/login');
		res.status(200).send();
	})
});


router.get('/heartbeat', (req, res) => {
	res.status(200).send();
});


module.exports = router;