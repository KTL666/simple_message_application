const auth = include('src/util/auth')

module.exports = (socket, next) => {
    function on_unauthorizaed() {
        socket.sendEndMessage({ message: 'Unauthorized!' })
    }
    function on_token_expire() {
        socket.sendEndMessage({ message: 'token expire' })
    }
    auth(socket.handshake.headers.authorization, socket.request, next, on_unauthorizaed, on_token_expire)
}
