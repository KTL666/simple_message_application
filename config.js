var crypto = require('crypto');

var config = {};

config.auth = {};
config.auth.jwt_expiry_ms = 60 * 60 * 1000;
config.auth.salt = 'salt' // crypto.randomBytes(16).toString('hex'); 
config.auth.iterations = 1000
config.auth.keylen = 64
config.auth.digest = 'sha512'
config.auth.private_key = 'private_key'

module.exports = config;