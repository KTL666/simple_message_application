var express = require('express');
var Chatroom = include('src/services/db/models/chatroom');
var User = include('src/services/db/models/user');
var router = express.Router();

router.get('/:name', (req, res) => {
    Chatroom.findOne({ name: req.params.name }, (err, chatroom) => {
        if (chatroom === null) {
            return res.status(400).send({
                message: "Chatroom not found."
            });
        }
        else {
            res.status(200).send(chatroom);
        }
    });
});

router.post('/', (req, res) => {
    var newChatroom = new Chatroom({
        name: req.body.name,
        member_ids: req.body.member_ids,
        message: [],
    });
    newChatroom.save((err, Chatroom) => {
        if (err)
            res.status(500).json("Database error");
        else
            req.body.member_ids.forEach((id) => {
                User.findOneAndUpdate(
                    { id },
                    {
                        $push: {
                            chatrooms: req.body.name
                        }
                    }, (err, response) => {
                        if (err) {
                            res.status(500).json(err);
                        } else {
                            res.status(200).send();
                        }
                    });
            })
    });
});

router.post('/member', (req, res) => {
    Chatroom.update({ name: req.body.chatroom_name }, { $push: { members: req.body.member_id } }, (err, response) => {
        if (err) {
            res.status(500).json(err);
        } else {
            res.status(200).send();
        }
    });
});


module.exports = router;