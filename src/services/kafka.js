const { Kafka } = require('kafkajs')
var JsonSocket = require('json-socket');
var User = include('src/services/db/models/user');
var Chatroom = include('src/services/db/models/chatroom');


function save_message(socket, group_name, sender, content, kafka_producer) {
    Chatroom.findOneAndUpdate(
        { name: group_name },
        {
            $push: {
                messages: { sender, content }
            }
        }, (err, response) => {
            if (err) {
                socket.sendEndMessage(err)
            } else {
                kafka_producer.send({
                    topic: group_name,
                    messages: [
                        { value: JSON.stringify({ sender, content }) },
                    ]
                })
            }
        });
}

module.exports = (socket) => {
    const uuid = socket.request.uuid
    const kafka = new Kafka({
        clientId: socket.id,
        brokers: [`${process.env.KAFKA_HOST}:${process.env.KAFKA_PORT}`],
    })

    const kafka_producer = kafka.producer()
    const kafka_consumer = kafka.consumer({ groupId: `${uuid} ${new Date()}`})

    User.findOne(
        { id: uuid }, (err, response) => {
            if (err) {
                socket.sendEndMessage(err)
            } else {

                kafka_producer.connect().then(() => {
                    const json_socket = new JsonSocket(socket)
                    json_socket.on('message', data => {
                        save_message(socket, data.group_name, uuid, data.content, kafka_producer)
                    })
                });

                kafka_consumer.connect().then(() => {
                    kafka_consumer.subscribe({ topics: response.chatrooms })
                    kafka_consumer.run({
                        eachMessage: async ({ topic, partition, message }) => {
                            socket.emit('message', {
                                chatroom: topic,
                                content: JSON.parse(message.value.toString()),
                            })
                        },
                    })
                });
            }
        });

}