var { mongoose } = include('src/services/connection')

var userSchema = mongoose.Schema({
    name: String,
    password: String,
    chatrooms: [String]
}, { timestamps: true });
var User = mongoose.model("users", userSchema);

module.exports = User;